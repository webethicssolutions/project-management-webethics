export function AllUserOptions(){
	
	const options = [
				  { value: 'amit', label: 'Amit' },{ value: 'rohit', label: 'Rohit' },{ value: 'harsh', label: 'Harsh' },{ value: 'rajesh', label: 'Rajesh' },{ value: 'kamal', label: 'Kamal' },{ value: 'vikas', label: 'Vikas' },{ value: 'anita', label: 'Anita' },{ value: 'anish', label: 'Anish' },{ value: 'deepak', label: 'Deepak' },{ value: 'sanjay', label: 'Sanjay' },{ value: 'amitchauhan', label: 'Amit Chauhan' },{ value: 'pallavi', label: 'Pallavi' },{ value: 'manju', label: 'Manju'},{ value: 'ravneet', label: 'Ravneet'} ,{ value: 'rishab', label: 'Rishab' },
				  
				];
	return options
}
export function AllProjectOptions(){
	const proejctOptions = [
	  { value: 'Project-195', label: 'Website Backup Bot (Cloud Backup Service)' },{ value: 'Project-196', label: 'Clown Cones & Confections (divi website build)' },{ value: 'Project-197', label: 'Studio H2O (divi website build)' },{ value: 'Project-198', label: 'Latisha Hardy Dance & Company (divi website build)' },{ value: 'Project-199', label: 'Sasha Bella Spa and Lashbar (RTO)' },{ value: 'Project-180', label: 'Clown Cones & Confections (divi website build)' },{ value: 'Project-185', label: 'Jay Smith & Associates Insurance Inc (divi website build)' },{ value: 'Project-187', label: 'Washout Watchdog (divi website build)' },{ value: 'Project-186', label: 'Acacia M&E, Inc (divi website build)' },{ value: 'Project-180', label: 'Farmers Feed and Seed (divi website build)' },{ value: 'Project-179', label: 'APW Cogan Custom (divi website build)' },
	];
	return 	proejctOptions;			
}					

export function selectedAssignedTo(){
	const assignedTo = [
						{ label: "Rajesh",value: 2 },{ label: "Kamal",value: 3 }
					   ];
	return assignedTo;
}
export function taskStatus(){
	const taskStatus = [
						{ label: "Completed",value: 1 },{ label: "In Progress",value: 2 },{ label: "Testing",value: 3 },{ label: "Pending",value: 4 }
					   ];
	return taskStatus;
	
}

export function validateProject(projectName) {
	const errors = [];  
	if (projectName.length <= 0) {
		errors.push("Please enter Project Name.");
	} 
   return errors;
}

export function validateComment(text) {
	const errors = [];  
	if (text !== '') {
		
		errors.push("Please enter Comments.");
	} 
   return errors;
}

export function validateTask(selectedOption,selectedUserOption,taskName) {
	const errors = [];  
	if (selectedOption.length <= 0) {
		errors.push("Please enter project name.");
	} 
	if (selectedUserOption.length <= 0) {
		errors.push("Please select users to assign the task.");
	} 
	if (taskName.length <= 0) {
		errors.push("Please enter task name.");
	} 
	console.log(errors);
   return errors;
}