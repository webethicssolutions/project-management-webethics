import React, { Component } from 'react'; 
import { Alert, Button, Card, CardBody, CardGroup, Col, Container, Input, InputGroup, InputGroupAddon, InputGroupText, Row} from 'reactstrap';

import { connect } from 'react-redux';
import { userlogin } from '../../../actions/simpleAction';

import { Redirect} from 'react-router-dom';
import logo from '../../../assets/img/brand/wethics_logo.svg'

 
function validate(email, upassword) {
	// we are going to store errors for all fields
	// in a signle array
	const errors = [];  
	if (email.length <= 0) {
		errors.push("Please enter email.");
		/* }else if (email.split('').filter(x => x === '@').length !== 1) { */
	}else if (email.length > 0) {
		/* errors.push("Email should contain a @ \n"); */ 
		let lastAtPos  = email.lastIndexOf('@');
		let lastDotPos = email.lastIndexOf('.');

		if (!(lastAtPos < lastDotPos && lastAtPos > 0 && email.indexOf('@@') === -1 && lastDotPos > 2 && (email.length - lastDotPos) > 2)) { 
			errors.push("Email is not valid.");
		} 
	} 
   if (upassword.length <= 0) {  
     errors.push("Please enter password.");
   }

  return errors;
}

class Login extends Component { 
	constructor(props) {
		super(props);
		this.state = {
			email: '',
			password: '',
			everFocusedEmail: false,
			everFocusedPassword: false,
			inFocus: '',
		};
		this.handleSubmit=this.handleSubmit.bind(this);
	}
  
    state = { showError: true }
  
	handleEmailChange = (evt) => {
		this.setState({ email: evt.target.value });
	}
  
	handlePasswordChange = (evt) => {
		this.setState({ password: evt.target.value });
	}

	componentDidMount() {
	   const { dispatch } = this.props
	   dispatch(userlogin());
	}
		
	componentDidUpdate(prevProps, prevState) {
	    if(prevProps.login_user_data!=this.props.login_user_data)
	    {	
	       if(this.props.login_user_data.success==true) {
	        this.props.history.push('/projects/projects');
	       } else {
	       	this.setState({ showLoginError: "" });
			this.setState((prevState, props) => { 
				return { showError: this.props.login_user_data.message } 
			 })  
	       }
	    }
  	}

	handleSubmit = (evt) => { 

		const { email, password } = this.state;
		   
		const errors = validate(email, password);
		if (errors.length > 0) { 
			this.setState({ showLoginError: "" });
			this.setState((prevState, props) => { 
				return { showError: errors } 
			 })  
		}else{ 
			var post={
		      email:this.state.email,
		      password:this.state.password
		    }
		    this.props.dispatch(userlogin(post));
		} 
    }
  
	
	render() { 
	console.log(localStorage.getItem('admin_token'));
		/* {localStorage.getItem('admin_token') && <div className="error-message"><Alert color="danger">{localStorage.getItem('admin_token')}</Alert></div>}  */
		const errors = validate(this.state.email, this.state.password);
		/* const isDisabled = Object.keys(errors).some(x => errors[x]); */
		return (
		  <div className="app flex-row align-items-center">
			<Container>
			  <Row className="justify-content-center">
				<Col md="8">
				  <CardGroup>
					<Card className="p-4 bg-primary ">
					  <CardBody>   
						<h2>Project Management</h2> 
						<p className="text-muted"></p>
						
					    {this.state.showLoginError && <div className="error-message"><Alert color="danger">{this.state.showLoginError}<br/></Alert></div>} 
						{this.state.showSuccess && <Redirect to="/tasks/tasks" />}
						{ /*<Form action="" method="post" encType="multipart/form-data" className="form-horizontal" onSubmit={this.loginSubmit.bind(this)}> */} 
						<form className="form-horizontal" onSubmit={this.handleSubmit} >
							 {this.state.showError && <p>{errors && errors.map(error => (
								<Alert color="danger" key={error}>{error}</Alert>
								))}</p>}
						<InputGroup className="mb-3">
						  <InputGroupAddon addonType="prepend">
							<InputGroupText>
							  <i className="icon-user"></i>
							</InputGroupText>
						  </InputGroupAddon>
						  <Input type="text" className={errors.email ? "error" : ""} placeholder="Enter email" value={this.state.email} onChange={this.handleEmailChange} />
						</InputGroup>
						<InputGroup className="mb-4">
						  <InputGroupAddon addonType="prepend">
							<InputGroupText>
							  <i className="icon-lock"></i>
							</InputGroupText>
						  </InputGroupAddon>
						  <Input type="password" className={errors.password ? "error" : ""} placeholder="Enter password" value={this.state.password} onChange={this.handlePasswordChange} />
						</InputGroup>
						<Row>
						  <Col xs="6">
							<Button color="primary " className="px-4 btn-dark">Login</Button>
						  </Col>
						  <Col xs="6" className="text-right">
						  {/* <Button color="link" className="px-0">Forgot password?</Button> */}
						  </Col>
						</Row>
						</form>
					  </CardBody>
					</Card>
					<Card className="text-white py-5 d-md-down-none" style={{ width: 44 + '%' }}>
					  <CardBody className="text-center pt-5">
						<div>
							<img src={logo} alt="" />
							
						</div>
					  </CardBody>
					</Card>
				  </CardGroup>
				</Col>
			  </Row>
			</Container>
		  </div>
		);
    }
}

function mapStateToProps(state) {
  const {commonReducer } = state
  const {login_user_data:login_user_data  } =  commonReducer
  console.log('res', login_user_data);
  return {login_user_data}
}

export default connect( mapStateToProps )(Login);;
