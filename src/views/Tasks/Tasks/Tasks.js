import React, { Component } from 'react';
import { Card, CardBody, CardHeader, Col, Row, Table} from 'reactstrap';

import tasksData from './TasksData'

import projectData from '../../Projects/Projects/ProjectsData'

var dateFormat = require('dateformat');

function TaskRow(props) { 
  const task = props.task
  const taskLink = `#/tasks/task/${task.id}`
  const taskDetailLink = `#/tasks/update-task/${task.id}`
  const nolink = `#`
  const newTaskLink = `#/tasks/add-task/`
   
 //const project = projectsData.find( project => project.id.toString() === task.project_id) 
  
  const project = projectData.find( project => project.id === task.project_id)
	console.log(project);
 const projectLink = `#/projects/project/${project.id}`
  return (
    <tr key={task.id.toString()}>
       <td><a href={taskLink}>{task.name}</a></td>  
        <td><a href={projectLink}>{project.name}</a></td>  
        <td>{task.created_by}</td> 
	</tr>
  )
}

class Tasks extends Component {
	
  render() {
		document.title = "My Tasks | Project Management";
	
		const taskList = tasksData.filter((task) => task.id < 10)
		const newTaskLink = `#/tasks/add-task/`
    return (
      <div className="animated fadeIn usr_list">
        <Row>
          <Col lg="12">
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i> Tasks
				
				<a href={newTaskLink} className="btn btn-primary float-right">Add Task</a>
              </CardHeader>
              <CardBody>
                <Table responsive hover striped>
                  <thead>
                    <tr>
                      <th scope="col">Task</th> 
					  <th scope="col">Project</th> 
					  <th scope="col">Reported To</th> 
                    </tr>
                  </thead>
                  <tbody>
                    {taskList.map((task, index) =>
                      <TaskRow key={index} task={task}/>
                    )}
                  </tbody>
                </Table> 
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    )
  }
}

export default Tasks;
