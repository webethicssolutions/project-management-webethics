const CommentsData = [
   {id: 1,project_id:1,task_id:1, comment: 'Edit Website to be "90% done"',commented_by:'Amit Sharma', commented_to: 'Vikas Kumar', commented_on: '2017-12-22 12:20:00PM'},
   {id: 2,project_id:1,task_id:2, comment: 'Edit Website to be "90% done"',commented_by:'Amit Sharma', commented_to: 'Vishal Kumar', commented_on: '2018-12-23 12:20:00PM'},
   {id: 3,project_id:2,task_id:1, comment: 'Edit Website to be "90% done"',commented_by:'Amit Sharma', commented_to: 'Rohit Kumar', commented_on: '2018-12-21 12:20:00PM'},
   {id: 4,project_id:3,task_id:2, comment: 'Edit Website to be "90% done"',commented_by:'Amit Sharma', commented_to: 'Anish Kumar', commented_on: '2018-12-18 12:20:00PM'},
   {id: 5,project_id:4,task_id:3, comment: 'Edit Website to be "90% done"',commented_by:'Amit Sharma', commented_to: 'Deepak Kumar', commented_on: '2018-12-18 12:20:00PM'},
   {id: 6,project_id:5,task_id:4, comment: 'Edit Website to be "90% done"',commented_by:'Amit Sharma', commented_to: 'Sanjay Kumar', commented_on: '2018-12-17 12:20:00PM'},
   {id: 7,project_id:5,task_id:5, comment: 'Edit Website to be "90% done"',commented_by:'Amit Sharma', commented_to: 'Harish Kumar', commented_on: '2018-12-16 12:20:00PM'},
   {id: 8,project_id:4,task_id:5, comment: 'Edit Website to be "90% done"',commented_by:'Amit Sharma', commented_to: 'Pallavi', commented_on: '2018-12-11 12:20:00PM'},
   {id: 9,project_id:2,task_id:2, comment: 'Edit Website to be "90% done"',commented_by:'Amit Sharma', commented_to: 'Manju', commented_on: '2018-12-14 12:20:00PM'},
   {id: 10,project_id:3,task_id:3, comment: 'Edit Website to be "90% done"',commented_by:'Amit Sharma', commented_to: 'Amit Chauhan', commented_on: '2018-12-26 12:20:00PM'},
   {id: 11,project_id:1,task_id:2, comment: 'Edit Website to be "90% done"',commented_by:'Amit Sharma', commented_to: 'Rishab Kumar', commented_on: '2018-12-27 07:20:00PM'},
]

export default CommentsData;
