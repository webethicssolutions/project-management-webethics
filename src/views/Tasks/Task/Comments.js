import React, { Component } from 'react';
//import { render } from 'react-dom';
import CommentsData from '../Tasks/CommentsData'

import * as ReactQuill from 'react-quill'; 

import Quill from 'quill/core';
import Toolbar from 'quill/modules/toolbar';
import Bubble from 'quill/themes/bubble';

import Bold from 'quill/formats/bold';
import Italic from 'quill/formats/italic';
import Header from 'quill/formats/header';
import Mention from 'quill-mention';

import 'quill/dist/quill.bubble.css';
import TimeAgo from 'react-timeago';
import * as util from '../../functions.js';

import { Alert,
	Button,
	Card,
	CardBody,
	CardHeader,
	Col,
	Form,
	FormGroup,
	Table
 } from 'reactstrap';  


const atValues = [
  { id: 1, value: 'Amit Sharma' },
  { id: 2, value: 'Rajesh Kumar' }
];
const hashValues = [
  { id: 3, value: 'Fredrik Sundqvist 2' },
  { id: 4, value: 'Patrik Sjölin 2' }
]
var i;
function CommentRow(props) { 
  const comment = props.comment
  
   return (
    <tr key={comment.id.toString()}>
	
        <td>
			<p style={{float:'left',marginRight:'5px'}}><img src={'assets/img/avatars/7.jpg'} className="img-avatar" alt="admin@bootstrapmaster.com" /></p>
			<a href="javascript:void(0)">{comment.commented_by}</a> <TimeAgo date={comment.commented_on} />
			<p><a href="javascript:void(0)">{comment.commented_to}</a> {comment.comment}</p>
			
		</td>  
		
   </tr>
  )
}
Quill.register({
		  'modules/toolbar': Toolbar,
		  'themes/bubble': Bubble,
		  'formats/bold': Bold,
		  'formats/italic': Italic,
		  'formats/header': Header
		});
class Comments extends Component {
	
	constructor(props) {
		super(props);
		this.state={
			text:null,
		
		}
		this.handleChange = this.handleChange.bind(this)
	}
	
	modules = {
		toolbar: [
		  [{ 'header': [1, 2, false] }],
		  ['bold', 'italic', 'underline','strike', 'blockquote'],
		  [{'list': 'ordered'}, {'list': 'bullet'}, {'indent': '-1'}, {'indent': '+1'}],
		  ['link', 'image'],
		  ['clean']
		],
		mention: {
          allowedChars: /^[A-Za-z\sÅÄÖåäö]*$/,
          mentionDenotationChars: ["@", "#"],
          source: function (searchTerm, renderList, mentionChar) {
            let values;
			
            if (mentionChar === "@") {
              values = atValues;
            } else {
              values = hashValues;
            }

            if (searchTerm.length === 0) {
              renderList(values, searchTerm);
            } else {
              const matches = [];
              for (i = 0; i < values.length; i++)
                if (~values[i].value.toLowerCase().indexOf(searchTerm.toLowerCase())) matches.push(values[i]);
              renderList(matches, searchTerm);
            }
          },
        }
	  }
	  handleChange(value) {
		this.setState({ text: value })
	  }
	
	handleSubmit = (evt) => { 
		const { text } = this.state;
		const errors = util.validateComment(text);
		if (errors.length > 0) { 
			this.setState({ showLoginError: "" });
			this.setState((prevState, props) => { 
				return { showError: errors } 
			 })  
		}else{ 
			
		} 
		  
	}
	render() {
		 /* {localStorage.getItem('admin_token') && <div className="error-message"><Alert color="danger">{localStorage.getItem('admin_token')}</Alert></div>}  */
		const errors = util.validateComment(this.state.text);
		
		const commentList = CommentsData.filter((comment) => comment.id < 15)
		return (
			
			 <Card>
				<CardHeader>
					<strong>Comments</strong>
				  </CardHeader>
				  <CardBody style={{height:"630px",overflow:"scroll"}}>
				   <Table responsive hover striped>
				    <tbody>
					{commentList.map((comment, index) =>
                      <CommentRow key={index} comment={comment}/>
                    )}
					 </tbody>
					</Table> 
					<Form encType="multipart/form-data" className="form-horizontal" onSubmit={this.handleSubmit}>
					<FormGroup row><Col xs="12" md="12">
					{this.state.showError && errors && errors.map(error => (
								<Alert color="danger" key={error}>{error}</Alert>
								))}
					</Col></FormGroup> 	
					<FormGroup row style={{height:"120px"}}>
						<Col xs="12" md="12">
						<ReactQuill modules={this.modules} value={this.state.text} onChange={this.handleChange} style={{height:"100px"}} >
							
						</ReactQuill>
							
						</Col>
					</FormGroup> 
					<FormGroup row>
					<div className="clearfix"></div>
					</FormGroup> 
					<FormGroup row>
						<Col xs="12" md="12">
							<Button color="primary " className="px-4">Comment</Button>
						</Col>
					</FormGroup> 
					</Form>
				</CardBody>
			</Card> 
			
		
		)
	}
	
}
export default Comments;