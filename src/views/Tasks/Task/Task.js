import React, { Component } from 'react';
import EditableLabel from 'react-inline-editing';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Comments from './Comments';
import taskData from '../Tasks/TasksData'
import Select from 'react-select';
import projectData from '../../Projects/Projects/ProjectsData'
import * as util from '../../functions.js';

import { 
	Card,
	CardBody,
	
	CardHeader,
	Col,
	
	Row,
	Table
 } from 'reactstrap';  


		
class Task extends Component {
	constructor(props){
		super(props);

		this._handleFocus = this._handleFocus.bind(this);
		this._handleFocusOut = this._handleFocusOut.bind(this);
		this.state = {
			startDate: '2019-04-11T00:00:00.000Z',
			endDate: '2019-08-11T00:00:00.000Z'
		};
		this.handleStartChange = this.handleStartChange.bind(this);
		this.handleEndChange = this.handleEndChange.bind(this);
	}
	
	handleStartChange(date) {
		this.setState({
		 	startDate: date
		});
	}
	  handleEndChange(date) {
		this.setState({
		 	endDate: date
		});
	}
	  
    _handleFocus(text) {
       /*  modules: this.modules */
    }

    _handleFocusOut(text) {
        console.log('Left editor with text: ' + text);
    }
	handleSelect(date){
        console.log(date); // Momentjs object
    }
  render() {
	document.title = "My Task | Project Management";
	
    const task = taskData.find( task => task.id.toString() === this.props.match.params.id)
	
	const taskDetails = task ? Object.entries(task) : [['id', (<span><i className="text-muted icon-ban"></i> Not found</span>)]]
	
	const project = projectData.find( project => project.id === task.project_id)
	
	return (
      <div className="animated fadeIn">
        <Row>
          <Col lg={6}>
            <Card>
              <CardHeader>
                <strong>{task.name}</strong>
              </CardHeader>
              <CardBody>
                  <Table responsive striped hover>
                    <tbody>
                      {
						taskDetails.map(([key, value]) => {
							if(key === "id" || key === "project_id"){
								if(key === "project_id"){
									return (
										<tr>
										  <td> <Select options={util.AllProjectOptions()} defaultValue={{ label: "Clown Cones & Confections (divi website build)",value: "Project-180" }} /></td>
										</tr>
									 )
								}
								
								
							}else{
								
								if(key === "name" || key === "description"){
									return (
										<tr>
										  
										 <td>
											<EditableLabel text={value}
												labelClassName='myLabelClass'
												inputClassName='myInputClass'
												inputWidth='100%'
												inputHeight='30px'
												labelFontWeight='bold'
												inputFontWeight='bold'
												onFocus={this._handleFocus}
												onFocusOut={this._handleFocusOut}
											/>
											
										  </td>
										</tr>
									  )
								}else if(key === "start_date"){
									
									return (
										<tr>
										  <td>
											 <DatePicker 
												selected={Date.parse(this.state.endDate)}
												onChange={this.handleStartChange}
												placeholderText="Start Date"
												minDate={new Date()}
												className="start_date form-control"
												dateFormat="MMMM d, yyyy"
											  />
											 
											 <DatePicker
												selected={Date.parse(this.state.endDate)}
												onChange={this.handleEndChange}
												style={{margin:"10px"}}
												placeholderText="End Date"
												minDate={new Date()}
												className="end_date form-control"
												dateFormat="MMMM d, yyyy"
											  />
											
										  </td>
										
										</tr>
									  )
									  
								}else if(key === 'assigned_to'){
									return (
										<tr>
											<td>
											<Select isMulti options={util.AllUserOptions()} defaultValue={util.selectedAssignedTo()}/>
											</td>
										</tr>
									)
								}else if(key === 'status'){
									return (
										<tr>
											<td>
											<Select options={util.taskStatus()} defaultValue={{ label: "Pending",value: 4}}/>
											</td>
										</tr>
									)
								}
							}
                        })
                      }
                    </tbody>
                  </Table>
              </CardBody>
            </Card>
          </Col>
		  <Col lg={6}>
			<Comments />
		</Col>
        </Row>
      </div>
    )
  }
}

export default Task;
