import React, { Component } from 'react';
import { render } from 'react-dom';
import CommentsData from '../Tasks/CommentsData'
/* import 'react-quill/dist/quill.snow.css'; */

/* import Select from 'react-select'; */
/* import * as ReactQuill from 'react-quill'; */
import TimeAgo from 'react-timeago';

//import 'react-time-ago/Tooltip.css';
import usersData from '../../Users/Users/UsersData'
import InputTrigger from 'react-input-trigger';
import {Editor, EditorState} from 'draft-js';


 
import { 
	Badge,
	Button,
	ButtonDropdown, 
	Card,
	CardBody,
	CardFooter,
	CardHeader,
	Col,
	Collapse, 
	Fade,
	Form,
	FormGroup,
	FormText,
	FormFeedback,
	Input,
	InputGroup,
	InputGroupAddon,
	InputGroupText,
	Label,
	Row,
	Table
 } from 'reactstrap';  
 
 var dateFormat = require('dateformat');

function CommentRow(props) { 
  const comment = props.comment
  
   return (
    <tr key={comment.id.toString()}>
	
        <td>
			<a href="#">{comment.commented_by}</a> <TimeAgo date={comment.commented_on} />
			<p><a href="#">{comment.commented_to}</a> {comment.comment}</p>
			<p>{comment.commented_on}</p>
			
		</td>  
		
   </tr>
  )
}

class Comments extends Component {
	
	constructor(props) {
		super();
		this.state = {
			  top: null,
			  left: null,
			  showSuggestor: false,
			  startPosition: null,
			  users: [
				'amit',
				'sharma',
				'india',
				'vikas',
				'bamit',
				'csharma',
				'dindia',
				'evikas'
			  ],
			  text: null,
			  currentSelection: 0,
			  editorState: EditorState.createEmpty()
		}
		/* this.toggleSuggestor = this.toggleSuggestor.bind(this);
		this.handleInput = this.handleInput.bind(this);
		this.handleKeyDown = this.handleKeyDown.bind(this);
		this.handleTextareaInput = this.handleTextareaInput.bind(this); */
		this.onChange = (editorState) => this.setState({editorState});
	}
	/* toggleSuggestor(metaInformation) {
		const { hookType, cursor } = metaInformation;

		if (hookType === 'start') {
		  this.setState({
			showSuggestor: true,
			left: cursor.left,
			top: cursor.top + cursor.height, // we need to add the cursor height so that the dropdown doesn't overlap with the `@`.
			startPosition: cursor.selectionStart,
		  });
		}
		
		if (hookType === 'cancel') {
		  // reset the state

		  this.setState({
			showSuggestor: false,
			left: null,
			top: null,
			text: null,
			startPosition: null,
		  });
		}
	  }
	  
	  handleInput(metaInformation) {
		this.setState({
		  text: metaInformation.text,
		});
	  }
	  
	  handleKeyDown(event) {
		const { which } = event;
		const { currentSelection, users } = this.state;
		
		if (which === 40 ) { // 40 is the character code of the down arrow
		  event.preventDefault();
		  
		  this.setState({
			currentSelection: (currentSelection + 1) % users.length,
		  });
		}
		
		if (which === 13) { // 13 is the character code for enter
		  event.preventDefault();
		  
		  const { users, currentSelection, startPosition, textareaValue } = this.state;
		  const user = users[currentSelection];
		  
		  const newText = `${textareaValue.slice(0, startPosition - 1)}${user}${textareaValue.slice(startPosition + user.length, textareaValue.length)}`
		  
		  // reset the state and set new text

		  this.setState({
			showSuggestor: false,
			left: null,
			top: null,
			text: null,
			startPosition: null,

			textareaValue: newText,
		  });
		  
		  this.endHandler();
		}
	  }
	  
	  handleTextareaInput(event) {
		const { value } = event.target;
		
		this.setState({
		  textareaValue: value,   
		})
	} */

	render() {
		const userList = usersData; 
		const commentList = CommentsData.filter((comment) => comment.id < 15)
		return (
			
			 <Card>
				<CardHeader>
					<strong>Comments</strong>
				  </CardHeader>
				  <CardBody>
				   <Table responsive hover striped>
				    <tbody>
					{commentList.map((comment, index) =>
                      <CommentRow key={index} comment={comment}/>
                    )}
					 </tbody>
					</Table> 
					<FormGroup row>
						<Col xs="12" md="12">
							/* <div
								style={{
								  position: 'relative'        
								}}
								
								onKeyDown={this.handleKeyDown}
							  >
								<InputTrigger
								  trigger={{
									keyCode: 50,
									shiftKey: true,
								  }}
								  onStart={(metaData) => { this.toggleSuggestor(metaData); }}
								  onCancel={(metaData) => { this.toggleSuggestor(metaData); }}
								  onType={(metaData) => { this.handleInput(metaData); }}
								  endTrigger={(endHandler) => { this.endHandler = endHandler; }}
								>
								  <textarea
									style={{
									  height: '100px',
									  width: '100%',
									  lineHeight: '1em',
									}}
									
									onChange={this.handleTextareaInput}
									value={this.state.textareaValue}
								  />
								 
								</InputTrigger>
								
								<div
								  id="dropdown"
								  style={{
									position: "absolute",
									width: "200px",
									borderRadius: "6px",
									background: "white",
									boxShadow: "rgba(0, 0, 0, 0.4) 0px 1px 4px",
												 
									display: this.state.showSuggestor ? "block" : "none",
									top: this.state.top,
									left: this.state.left,
								  }}
								>
								  {
									this.state.users
									  .filter(user => user.indexOf(this.state.text) !== -1)
									  .map((user, index) => (
										<div
										  style={{
											padding: '10px 20px',
											background: index === this.state.currentSelection ? '#eee' : ''
										  }}
										>
										  { user }
										</div>
									  ))  
								  }
								</div>
						</div>
						 */
						 <Editor editorState={this.state.editorState} onChange={this.onChange} />
						</Col>
						
						 
					</FormGroup> 
				</CardBody>
			</Card> 
			
		
		)
	}
	
}
export default Comments;