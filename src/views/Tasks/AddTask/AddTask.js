import React, { Component } from 'react';
import Spinner from '../../ImageUpload/Spinner';
import Images from '../../ImageUpload/Images';
import Buttons from '../../ImageUpload/Buttons';
import '../../../scss/upload.css';
import 'react-quill/dist/quill.snow.css';

import Select from 'react-select';
import * as ReactQuill from 'react-quill';
import * as util from '../../functions.js';

 import { Alert,
	Button,
	Card,
	CardBody,
	CardFooter,
	CardHeader,
	Col,
	Form,
	FormGroup,
	Input,
	Row
 } from 'reactstrap';  
 
class AddProject extends Component { 

		 constructor(props) {
			super(props)
			this.handleChange = this.handleChange.bind(this)
		  }
		  handleChange(value) {
			this.setState({ text: value })
		  }
		  
		state = {
			uploading: false,
			images: [],
			text: '',
			selectedOption: '',
			selectedUserOption: '',
			taskName:'',
		}
		onChange = e => {
			const files = Array.from(e.target.files)
			this.setState({ uploading: true })

			const formData = new FormData()

			files.forEach((file, i) => {
			  formData.append(i, file)
			})

			fetch(`/image-upload`, {
			  method: 'POST',
			  body: formData
			})
			.then(res => res.json())
			.then(images => {
			  this.setState({ 
				uploading: false,
				images
			  })
			})
		}
		removeImage = id => {
			this.setState({
			  images: this.state.images.filter(image => image.public_id !== id)
			})
		}
		handletaskNameChange = (evt) => {
			this.setState({ taskName: evt.target.value });
		}
		handleSubmit = (evt) => { 
			
			const { selectedOption, selectedUserOption,taskName } = this.state;
			
			const errors = util.validateTask(selectedOption,selectedUserOption,taskName);
			//console.log(selectedOption);
			if (errors.length > 0) { 
				this.setState({ showLoginError: "" });
				this.setState((prevState, props) => { 
					return { showError: errors } 
				 })  
			}else{ 
				
			} 
		}
		handleSelectedChange = (selectedOption) => {
			this.setState({ selectedOption });
		}
		
		handleUserChange = (selectedUserOption) => {
			this.setState({ selectedUserOption });
		}
		
	render() {
		document.title = "Add New Task | Project Management";
		
		const { uploading, images } = this.state
		
		const content = () => {
			  switch(true) {
				case uploading:
				  return <Spinner />
				case images.length > 0:
				  return <Images images={images} removeImage={this.removeImage} />
				default:
				  return <Buttons onChange={this.onChange} />
			  }
		}
		
		/* {localStorage.getItem('admin_token') && <div className="error-message"><Alert color="danger">{localStorage.getItem('admin_token')}</Alert></div>}  */
		const errors = util.validateTask(this.state.selectedOption,this.state.selectedUserOption,this.state.taskName);
		const { selectedOption } = this.state; 
		const { selectedUserOption } = this.state; 
		return (
		  <div className="animated fadeIn add_usr">
			<Row>
			  <Col md="12">
				<Card>
				  <CardHeader>
					<strong>Add New Task</strong>
				  </CardHeader>
				  <Form onSubmit={this.handleSubmit} encType="multipart/form-data" className="form-horizontal">
				  <CardBody>
					<FormGroup row><Col xs="12" md="3">
					{this.state.showError && errors && errors.map(error => (
								<Alert color="danger" key={error}>{error}</Alert>
								))}
					</Col></FormGroup> 			
					 <FormGroup row>
						<Col xs="12" md="9">
						 <Select options={util.AllProjectOptions()} placeholder="Select Project" value={selectedOption} onChange={this.handleSelectedChange} />
						</Col>
					  </FormGroup> 
					  
					  <FormGroup row>
						
						<Col xs="12" md="9">
						  <Input type="text" id="text-input" placeholder="Task Name" value={this.state.taskName} onChange={this.handletaskNameChange} /> 
						</Col>
					  </FormGroup>
					   <FormGroup row style={{"minHeight":"120px"}}>
						
						<Col xs="12" md="9">
						  <ReactQuill placeholder="Task Description" value={this.state.text} onChange={this.handleChange} style={{"height":"100px"}} />
						</Col>
					  </FormGroup>    
					  <FormGroup row>
						
						<Col xs="12" md="2">
						 <div className='buttons'>
									  {content()}
									</div>
						</Col>
					  </FormGroup> 
					  <FormGroup row>
						<Col xs="12" md="9">
						 <Select isMulti options={util.AllUserOptions()} placeholder="Assigned To..." value={selectedUserOption} onChange={this.handleUserChange}  /> 
						</Col>
					  </FormGroup> 
					
				  </CardBody>
				  <CardFooter>
					<Button size="sm" color="primary"><i className="fa fa-dot-circle-o"></i> Submit</Button>
				  </CardFooter>
				  </Form>
				</Card> 
			  </Col> 
			</Row>
		  </div>
		)
	}
}

export default AddProject;
