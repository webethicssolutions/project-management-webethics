import React, { Component } from 'react';
import { Card, CardBody, CardHeader, Col, Row, Table} from 'reactstrap';
import projectsData from './ProjectsData';

var dateFormat = require('dateformat');

function ProjectRow(props) { 
  const project = props.project
  const projectLink = `#/projects/project/${project.id}`
  const projectDetailLink = `#/projects/update-project/${project.id}`
  const nolink = `#`
  const newTaskLink = `#/tasks/add-task/`
 
  return (
    <tr key={project.id.toString()}>
        <td><a href={projectLink}>{project.name}</a></td>  
        <td><a href="#">{project.created_by}</a></td> 
	    <td><a href="#">1</a></td> 
	 </tr>
  )
}

class Projects extends Component {

  render() {
	document.title = "Projects | Project Management";
	const projectList = projectsData.filter((project) => project.id < 10)
	const newprojectLink = `#/projects/add-project/`
    return (
      <div className="animated fadeIn usr_list">
        <Row>
          <Col lg="12">
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i> Projects
				
				<a href={newprojectLink} className="btn btn-primary float-right">Add Project</a>
              </CardHeader>
              <CardBody>
                <Table responsive hover striped>
                  <thead>
                    <tr>
                      <th scope="col">Name</th> 
                      <th scope="col">Created By</th> 
					  <th scope="col">Total Tasks</th> 
					  
                    </tr>
                  </thead>
                  <tbody>
                    {projectList.map((project, index) =>
                      <ProjectRow key={index} project={project}/>
                    )}
                  </tbody>
                </Table> 
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    )
  }
}

export default Projects;
