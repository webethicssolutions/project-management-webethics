import React, { Component } from 'react';
import { Card, CardBody, CardHeader, Col, Row, Table } from 'reactstrap';
import ReactDOM from 'react-dom';
import EditableLabel from 'react-inline-editing';
import DatePicker from "react-datepicker";
import projectData from '../Projects/ProjectsData';
import "react-datepicker/dist/react-datepicker.css";
import Select from 'react-select';
import * as util from '../../functions.js';

var dateFormat = require('dateformat');

class Project extends Component {
	constructor(props){
		super(props);

		this._handleFocus = this._handleFocus.bind(this);
		this._handleFocusOut = this._handleFocusOut.bind(this);
		this.state = {
			startDate: '2019-04-11T00:00:00.000Z',
			endDate: '2019-08-11T00:00:00.000Z'
		};
		this.handleStartChange = this.handleStartChange.bind(this);
		this.handleEndChange = this.handleEndChange.bind(this);
		
	}
	
	handleStartChange(date) {
		this.setState({
		 	startDate: date
		});
	}
	handleEndChange(date) {
		this.setState({
		 	endDate: date
		});
	}
	  
	  
    _handleFocus(text) {
        console.log('Focused with text: ' + text);
    }

    _handleFocusOut(text) {
        console.log('Left editor with text: ' + text);
    }

	
  render() {
	
    const project = projectData.find( project => project.id.toString() === this.props.match.params.id)
	
	const projectDetails = project ? Object.entries(project) : [['id', (<span><i className="text-muted icon-ban"></i> Not found</span>)]]
	
	
	
	const newTaskLink = `#/tasks/add-task/`
	
											
    return (
      <div className="animated fadeIn">
        <Row>
          <Col lg={12}>
            <Card>
              <CardHeader>
                <strong>{project.name}</strong>
				<a href={newTaskLink} className="btn btn-primary float-right">Add New Task</a>
              </CardHeader>
              <CardBody>
                  <Table responsive striped hover>
                    <tbody>
                      {
                        projectDetails.map(([key, value]) => {
							console.log(key);
							
							if(key === "id" || key === "created_by"){
								
							}	
							if(key === "name" || key === "description"){
								return (
									<tr>
									  <td>
										<EditableLabel text={value}
											labelClassName='myLabelClass'
											inputClassName='myInputClass'
											inputWidth='100%'
											inputHeight='30px'
											labelFontWeight='bold'
											inputFontWeight='bold'
											onFocus={this._handleFocus}
											onFocusOut={this._handleFocusOut}
										/>
									  </td>
									</tr>
								  )
							}else if(key === "start_date"  || key === "end_date"){
								return (
									<tr>
									  
									  <td>
										<DatePicker 
											selected={Date.parse(this.state.startDate)}
											onChange={this.handleStartChange}
											placeholderText="Start Date"
											minDate={new Date()}
											className="start_date form-control"
											dateFormat="MMMM d, yyyy"
										  />
										 
										 <DatePicker
											selected={Date.parse(this.state.endDate)}
											onChange={this.handleEndChange}
											style={{margin:"10px"}}
											placeholderText="End Date"
											minDate={new Date()}
											className="end_date form-control"
											dateFormat="MMMM d, yyyy"
										  />
									  </td>
									</tr>
								  )
							}else if(key === 'status'){
								return (
									<tr>
										<td>
										<Select options={util.taskStatus()} defaultValue={{ label: "Pending",value: 4}}/>
										</td>
									</tr>
								)
							}
						})
                      }
                    </tbody>
                  </Table>
				 
              </CardBody>
			  
            </Card>
          </Col>
        </Row>
      </div>
    )
  }
}

export default Project;
