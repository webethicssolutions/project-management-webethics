import React, { Component } from 'react';
import Spinner from '../../ImageUpload/Spinner';
import Images from '../../ImageUpload/Images';
import Buttons from '../../ImageUpload/Buttons';
import '../../../scss/upload.css';
import { Redirect} from 'react-router-dom';
import 'react-quill/dist/quill.snow.css';
import * as ReactQuill from 'react-quill';
import * as util from '../../functions.js';


 import {  Alert,
	Button,
	Card,
	CardBody,
	CardFooter,
	CardHeader,
	Col,
	Form,
	FormGroup,
	FormText,
	Input,
	Row
 } from 'reactstrap';  
 
class AddProject extends Component {  
		constructor(props) {
			super(props)
			this.handleChange = this.handleChange.bind(this)
		  }
		  handleChange(value) {
			this.setState({ text: value })
		  }
		 handleprojectNameChange = (evt) => {
			this.setState({ projectName: evt.target.value });
		} 
		state = {
			uploading: false,
			images: [],
			text: '',
			showError: false,
			projectName: '',
		}
		onChange = e => {
			const files = Array.from(e.target.files)
			this.setState({ uploading: true })

			const formData = new FormData()

			files.forEach((file, i) => {
			  formData.append(i, file)
			})

			fetch(`/image-upload`, {
			  method: 'POST',
			  body: formData
			})
			.then(res => res.json())
			.then(images => {
			  this.setState({ 
				uploading: false,
				images
			  })
			})
		}
		
		removeImage = id => {
			this.setState({
			  images: this.state.images.filter(image => image.public_id !== id)
			})
		}
		handleSubmit = (evt) => { 
			
			const { projectName } = this.state;
			  
			const errors = util.validateProject(projectName);
			if (errors.length > 0) { 
				this.setState({ showLoginError: "" });
				this.setState((prevState, props) => { 
					return { showError: errors } 
				 })  
			}else{ 
				
			} 
		}
		
	render() {
		document.title = "Add New Project | Project Management";
		const { uploading, images } = this.state

			const content = () => {
			  switch(true) {
				case uploading:
				  return <Spinner />
				case images.length > 0:
				  return <Images images={images} removeImage={this.removeImage} />
				default:
				  return <Buttons onChange={this.onChange} />
			  }
		}
		/*{localStorage.getItem('admin_token') && <div className="error-message"><Alert color="danger">{localStorage.getItem('admin_token')}</Alert></div>} */
		const errors = util.validateProject(this.state.projectName);
		return (
		
			
		  <div className="animated fadeIn add_usr">
			<Row>
			  <Col md="12">
				
				<Card>
				  <CardHeader>
					<strong>Add New Project</strong>
				  </CardHeader>
				  {this.state.showSuccess && <Redirect to="/projects/projects" />}
					<Form encType="multipart/form-data" className="form-horizontal" onSubmit={this.handleSubmit}>
				  <CardBody>
				  
						{this.state.showError && <p>{errors && errors.map(error => (
								<Col xs="12" md="9"><Alert color="danger" key={error}>{error}</Alert></Col>
								))}</p>}
					  <FormGroup row>
						<Col xs="12" md="9">
						  <Input type="text" id="text-input"  className={errors.projectName ? "error" : ""} value={this.state.projectName} onChange={this.handleprojectNameChange} placeholder="Project Name" /> 
						</Col>
					  </FormGroup>
					   <FormGroup row style={{"minHeight":"120px"}} >
						
						<Col xs="12" md="9">
						   <ReactQuill name="textarea-input" placeholder="Project Description"  value={this.state.text} onChange={this.handleChange} style={{"height":"100px"}} />
						</Col>
					  </FormGroup>    
					  <FormGroup row>
						
						<Col xs="12" md="2">
						 <div className='buttons'>
									  {content()}
									</div>
						</Col>
					  </FormGroup> 
					
					
				  </CardBody>
				  <CardFooter>
					<Button size="sm" color="primary"><i className="fa fa-dot-circle-o"></i> Submit</Button>
				  </CardFooter>
				  </Form>
				</Card> 
			  </Col> 
			</Row>
		  </div>
		)
	}
}

export default AddProject;
