export default {
  items: [
	{
      name: 'Projects',
      url: '/projects/projects',
      icon: 'icon-folder',
	}, {
      name: 'My Tasks',
      url: '/tasks/tasks',
      icon: 'icon-speedometer',
	 },
  ],
};
