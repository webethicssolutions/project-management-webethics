import React, { Component } from 'react';
import { Badge, DropdownItem, Dropdown,  DropdownMenu, DropdownToggle, Nav, NavItem, NavLink } from 'reactstrap';
import PropTypes from 'prop-types';
import Modal from 'react-responsive-modal';
import { AppAsideToggler, AppHeaderDropdown, AppNavbarBrand, AppSidebarToggler } from '@coreui/react';
import logo from '../../assets/img/brand/wethics_logo.svg'
import sygnet from '../../assets/img/brand/sygnet.svg'

import AddProject from '../../views/Projects/AddProject/AddProject';
import AddTask from '../../views/Tasks/AddTask/AddTask';

const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};

class DefaultHeader extends Component {
	
		constructor(props) {
			super(props);

			this.toggle = this.toggle.bind(this);
			this.state = {
			  dropdownOpen: false
			};
		  }
  
 

   toggle() {
    this.setState(prevState => ({
      dropdownOpen: !prevState.dropdownOpen
    }));
  }
  
  
  render() {

    // eslint-disable-next-line
    const { children, ...attributes } = this.props;
	
	
	 
	
    return (
      <React.Fragment>
        <AppSidebarToggler className="d-lg-none" display="md" mobile />
     
		<AppNavbarBrand
          full={{ src: logo, width: 139, height: 105, alt: 'Webethics Logo' }}
          minimized={{ src: sygnet, width: 30, height: 30, alt: 'Webethics Logo' }}
        />
		
		
        <AppSidebarToggler className="d-md-down-none" display="lg" />

        <Nav className="d-md-down-none" navbar>
          
          {/* <NavItem className="px-3">
            <NavLink href="#">Settings</NavLink>
          </NavItem> */}
        </Nav>
        <Nav className="ml-auto" navbar>
		  <AppHeaderDropdown direction="down">
			<Dropdown isOpen={this.state.dropdownOpen} toggle={this.toggle}>
				<DropdownToggle className="btn btn-primary mr-5 pl-5 pr-5 font-weight-bold">
				New
				</DropdownToggle>
				<DropdownMenu right style={{ right: 'auto' }}> {/*onClick={this.onOpenFirstModal} onClick={this.onOpenSecondModal}*/ }
				  <DropdownItem  href="#/projects/add-project"><i className="fa fa-user"></i> Project</DropdownItem>
				  <DropdownItem  href="#/tasks/add-task"><i className="fa fa-lock"></i> Task</DropdownItem>
				  <DropdownItem href="#/login"><i className="fa fa-sign-out"></i> Logout</DropdownItem>
				</DropdownMenu>
			</Dropdown>
            
          </AppHeaderDropdown>
        </Nav>
        {/* <AppAsideToggler className="d-md-down-none" /> */}
        {/*<AppAsideToggler className="d-lg-none" mobile />*/}
		
      </React.Fragment>
	  
	  
    );
  }
}

DefaultHeader.propTypes = propTypes;
DefaultHeader.defaultProps = defaultProps;

export default DefaultHeader;
