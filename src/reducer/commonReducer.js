import {RECEIVE_LOGIN} from '../actions/simpleAction';

function commonReducer(state = {
    login_user_data:[]  },action)
  {
  
      switch (action.type) 
      {
          case RECEIVE_LOGIN:
            return Object.assign({}, state, 
              {
                login_user_data: action.login_user_data
            })
  
            default:
            return state
     }
  }
  export default commonReducer