import { combineReducers } from 'redux';
import simpleReducer from './simpleReducer';
import commonReducer from './commonReducer';

export default combineReducers({
 simpleReducer,
 commonReducer
});